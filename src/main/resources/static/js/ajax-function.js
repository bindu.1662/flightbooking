$(document).ready(function () {
   $("#flight-booking-form").submit(function (event) {
       //stop submit the form event. Do this manually using ajax post function
       event.preventDefault();
       var flightBookingForm = {}
       flightBookingForm["sourceName"] = $("#sourceName").val();
       flightBookingForm["destinationName"] = $("#destinationName").val();
       flightBookingForm["numberOfPassengers"] = $("#numberOfPassengers").val();
       $("#btn-search").prop("disabled", true);
       $.ajax({
           type: "POST",
           contentType: "application/json",
           url: "/api/search",
           data: JSON.stringify(flightBookingForm),
           dataType: 'json',
           cache: false,
           timeout: 600000,
           success: function (data) {
               var json = "<h4>Ajax Response</h4><pre>"
                   + JSON.stringify(data, null, 4) + "</pre>";
               $('#feedback').html(json);
               console.log("SUCCESS : ", data);
               $("#btn-search").prop("disabled", false);
           },
           error: function (e) {
               var json = "<h4>Ajax Response Error</h4><pre>"
                   + e.responseText + "</pre>";
               $('#feedback').html(json);
               console.log("ERROR : ", e);
               $("#btn-search").prop("disabled", false);
           }
       });
   });
});