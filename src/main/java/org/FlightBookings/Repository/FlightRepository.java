package org.FlightBookings.Repository;

import org.FlightBookings.Model.FlightModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
@Repository
public class FlightRepository implements ObjectRepository<FlightModel> {
    private Map<String, FlightModel> repository;

    public FlightRepository() {
        this.repository = new HashMap<String, FlightModel>();
    }

    public void loadFLights() {
        repository.put("BOE777-01", new FlightModel("BOE777-01", "BOEING777", 100, "DELHI", "HYDERABAD"));
        repository.put("A319-01", new FlightModel("A319-01", "Airbus A319 ", 100, "DELHI", "HYDERABAD"));
        repository.put("A321-01", new FlightModel("A321-01", "Airbus A321", 100, "DELHI", "HYDERABAD"));
        repository.put("A321-02", new FlightModel("A321-02", "Airbus A321", 100, "MUMBAI", "CHENNAI"));
        repository.put("A321-03", new FlightModel("A321-03", "Airbus A321", 5, "CHENNAI", "HYDERABAD"));
    }

    ArrayList<FlightModel> listOfFlights() {
        loadFLights();
        return new ArrayList<FlightModel>(repository.values());
    }

    @Override
    public ArrayList<FlightModel> getFlights() {
        loadFLights();
        return listOfFlights();
    }

    @Override
    public FlightModel retrieve(String flightNumber) {
        loadFLights();
        return repository.get(flightNumber);
    }

    @Override
    public FlightModel search(String source, String destination, int numberOfPassenger) {
        loadFLights();
        Collection<FlightModel> flights = repository.values();
        for (FlightModel flightModel : flights) {
            if ((flightModel.getSourceName().equalsIgnoreCase(source)) && (flightModel.getDestinationName().equalsIgnoreCase(destination))) {
                if (flightModel.getAvailbleSeats() != 0) {
                    return flightModel;
                } else {
                    return null;
                }
            }
        }
        return null;
    }
}

