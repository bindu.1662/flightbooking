package org.FlightBookings.Repository;

import java.util.ArrayList;

public interface ObjectRepository<FlightModel> {
    //    public void store(FlightModel t);
//
    public FlightModel retrieve(String id);
    //
    public FlightModel search(String source,String destination,int noOfPassengers);
    //
//    public FlightModel delete(String id);
    public ArrayList<FlightModel> getFlights ();
}





