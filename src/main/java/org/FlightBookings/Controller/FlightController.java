package org.FlightBookings.Controller;

import org.FlightBookings.Model.AjaxResponseBody;
import org.FlightBookings.Model.FlightBookingForm;
import org.FlightBookings.Model.FlightModel;
import org.FlightBookings.Repository.FlightRepository;
import org.FlightBookings.Services.FlightService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FlightController{
 FlightRepository repository = new FlightRepository ();
         FlightService service = new FlightService ( repository );
//    @GetMapping("/")
//    public String index () {
//        return "FlightSelection";
//    }
@ModelAttribute("availableSources")
public ArrayList <String> getAvaialbleSources () {
        ArrayList <String> availableSources = new ArrayList <String> ();
        availableSources = service.getAllSources ();
        return availableSources;
        }
@ModelAttribute("availableDestinations")
public ArrayList <String> getAvaialbleDestinations () {
        ArrayList <String> availableDestinations = new ArrayList <String> ();
        availableDestinations = service.getAllSources ();
        return availableDestinations;
        }
@RequestMapping("/search")
public ArrayList <FlightModel> getAllFlights () {
        return (service.searchFlight ());
        }
@RequestMapping("/flight/{flightNumber}")
public FlightModel getFlightByNumber ( @PathVariable String flightNumber ) {
        return (service.getFlightByFlightNumber ( flightNumber ));
        }
@RequestMapping("/flight")
public ArrayList <FlightModel> searchFlightBasedOnSourceAndDestination ( @RequestParam String source
        , @RequestParam String destination , @RequestParam int noOfPassenger ) {
        return service.searchFlightBasedOnSourceAndDestination ( source , destination , noOfPassenger );
        }
@PostMapping("/api/search")
public ResponseEntity<?> getSearchResultViaAjax (@Valid @RequestBody FlightBookingForm flightBookingForm , Errors errors ) {
        AjaxResponseBody result = new AjaxResponseBody ();
        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors ()) {
        result.setMsg ( errors.getAllErrors ()
        .stream (). <String>map ( x -> x.getDefaultMessage () )
        .collect ( Collectors.joining ( "," ) ) );
        return ResponseEntity.badRequest ().body ( result );
        }
        List<FlightModel> flights = service.searchFlightBasedOnSourceAndDestinationNew ( flightBookingForm );
        if (flights.isEmpty ()) {
        result.setMsg ( "no flights found!" );
        } else {
        result.setMsg ( "success" );
        }
        result.setResult ( flights );
        return ResponseEntity.ok ( result );
        }
}
