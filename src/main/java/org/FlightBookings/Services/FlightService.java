package org.FlightBookings.Services;

import org.FlightBookings.Model.FlightBookingForm;
import org.FlightBookings.Model.FlightModel;
import org.FlightBookings.Repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;

public class FlightService {

    private FlightRepository repository;
    @Autowired
    public FlightService ( FlightRepository repository ) {

        this.repository = repository;
        this.repository.loadFLights ();
    }
    public FlightService () {
    }
    public ArrayList <FlightModel> searchFlight () {
        return this.repository.getFlights ();
    }
    public FlightModel getFlightByFlightNumber ( String flightNumber ) {
        return this.repository.retrieve ( flightNumber );
    }
    public ArrayList <FlightModel> searchFlightBasedOnSourceAndDestination ( String source , String destination , int noOfPassenger ) {
        // return this.repository.search ( source , destination ,noOfPassenger);
        //this.repository.loadFLights ();
        Collection<FlightModel> flights = this.repository.getFlights ();
        ArrayList <FlightModel> flightList = new ArrayList <FlightModel> ();
        for (FlightModel flightModel : flights) {
            if ((flightModel.getSourceName ().equalsIgnoreCase ( source )) && (flightModel.getDestinationName ().equalsIgnoreCase ( destination ))) {
                if (flightModel.getAvailbleSeats () > noOfPassenger) {
                    flightList.add ( flightModel );
                } else {
                    return flightList;
                }
            }
        }
        return flightList;
    }
    public ArrayList <FlightModel> searchFlightBasedOnSourceAndDestinationNew ( FlightBookingForm flightBookingForm ) {
        ArrayList <FlightModel> resultList = searchFlightBasedOnSourceAndDestination ( flightBookingForm.getSourceName () ,
                flightBookingForm.getDestinationName () ,
                flightBookingForm.getNumberOfPassengers () );
        return resultList;
    }
    public ArrayList <String> getAllSources () {
        ArrayList <FlightModel> flights = repository.getFlights ();
        ArrayList <String> availableSources = new ArrayList <String> ();
        for (FlightModel flight : flights) {
            availableSources.add ( flight.getSourceName () );
        }
        return availableSources;
    }
    public ArrayList<String> getAllDestinations () {
        ArrayList <FlightModel> flights = repository.getFlights ();
        ArrayList <String> availableDestinations = new ArrayList <String> ();
        for (FlightModel flight : flights) {
            availableDestinations.add ( flight.getDestinationName ());
        }
        return availableDestinations;
    }
}

