package org.FlightBookings.Model;

import javax.validation.constraints.NotEmpty;

public class FlightBookingForm {

    @NotEmpty(message = "Source name can't be empty!")
    String sourceName;

    @NotEmpty(message = "Destination name can't be empty!")
    String DestinationName;

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    int numberOfPassengers;

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public void setDestinationName(String destination) {
        DestinationName = destination;
    }

    public String getDestinationName() {
        return DestinationName;
    }


}
