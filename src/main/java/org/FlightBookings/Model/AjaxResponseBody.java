package org.FlightBookings.Model;

import java.util.List;

public class AjaxResponseBody {
    private String msg;
    private List<FlightModel> result;

    public List<FlightModel> getResult(){return result;}

    public void setResult(List<FlightModel> result) {
        this.result = result;
    }

    public String getMsg() { return msg;}

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
