package org.FlightBookings.Model;

import java.util.ArrayList;

public class FlightModel extends ArrayList<FlightModel> {
    private String flightNumber;
    private String flightModel;
    private int availbleSeats;
    private String sourceName;
    private String destinationName;
    public FlightModel ( String flightNumber , String flightModel , int availbleSeats , String sourceName , String destinationName ) {
        this.flightNumber = flightNumber;
        this.flightModel = flightModel;
        this.availbleSeats = availbleSeats;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
    }
    public FlightModel ( String flightNumber , String flightModel , int availbleSeats ) {
        this.flightNumber = flightNumber;
        this.flightModel = flightModel;
        this.availbleSeats = availbleSeats;
    }
    public FlightModel () {
    }
    public String getSourceName () {
        return sourceName;
    }
    public void setSourceName ( String sourceName ) {
        this.sourceName = sourceName;
    }
    public String getDestinationName () {
        return destinationName;
    }
    public void setDestinationName ( String destinationName ) {
        this.destinationName = destinationName;
    }
    public String getFlightNumber () {
        return flightNumber;
    }
    public void setFlightNumber ( String flightNumber ) {
        this.flightNumber = flightNumber;
    }
    public String getFlightModel () {
        return flightModel;
    }
    public void setFlightModel ( String flightModel ) {
        this.flightModel = flightModel;
    }
    public int getAvailbleSeats () {
        return availbleSeats;
    }
    public void setAvailbleSeats ( int availbleSeats ) {
        this.availbleSeats = availbleSeats;
    }

    @Override
    public  String toString(){
        return "Flight{" +
                "Model='" + flightModel +'\'' +
                ", Number ='" + flightNumber + '\''+
                ", Source ='" + sourceName +'\'' +
                ", Destination='" + destinationName + '\'' +
                '}';
    }


}