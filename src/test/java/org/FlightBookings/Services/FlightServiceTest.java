package org.FlightBookings.Services;

import org.FlightBookings.Model.FlightModel;
import org.FlightBookings.Repository.FlightRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class FlightServiceTest {
    FlightService service;
    FlightRepository repository;
    @BeforeEach
    public void load () {
        repository = new FlightRepository ();
        service = new FlightService (repository);
    }
    @Test
    public void whenSourceAndDestinationNotGivenSearch () {
        ArrayList<FlightModel> flightList = service.searchFlightBasedOnSourceAndDestination ( "" , "" , 0 );
        assertEquals(new ArrayList <> (  ), flightList );
    }
    @Test
    public void whenSourceAndDestinationNotGivenSearchWithPassengers () {
        ArrayList <FlightModel> flightList = service.searchFlightBasedOnSourceAndDestination ( "" , "" , 1 );
        assertEquals ( new ArrayList <> (  ) , flightList );
    }
    @Test
    public void whenSourceAndDestinationGivenSearchWithNoPassengers () {
        ArrayList <FlightModel> flightList = service.searchFlightBasedOnSourceAndDestination ( "MUMBAI" , "DELHI" , 0 );
        assertEquals ( flightList.size (),0   );
    }
    @Test
    public void whenSourceAndDestinationGivenSearchWithNoPassenger (){
        ArrayList <FlightModel> flightList = service.searchFlightBasedOnSourceAndDestination ( "DELHI" , "HYDERABAD" , 0 );
        assertEquals ( flightList.size (),3   );
    }
    @Test
    public void whenSourceAndDestinationGivenSearchWithPassengerLessThanAvaialableSeats (){
        ArrayList <FlightModel> flightList = service.searchFlightBasedOnSourceAndDestination ( "CHENNAI" , "HYDERABAD" , 10 );
        assertEquals ( flightList.size (),0  );
    }
}
